##  Copyright (C) 2014  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{res} =} ls_isempty (@var{phi})
## 
## Check if the set described by @var{phi} is the empty set.
##
## @seealso{ls_inside}
## @end deftypefn

function res = ls_isempty (phi)
  if (nargin () ~= 1)
    print_usage ();
  endif

  inside = ls_inside (phi);
  res = all (~inside(:));
endfunction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests.

% Test for error conditions.
%!error <Invalid call to>
%!  ls_isempty ()
%!error <Invalid call to>
%!  ls_isempty (1, 2)

% Basic tests for the function.
%!test
%!  n = 50;
%!  x = linspace (-10, 10, n);
%!  [XX, YY] = meshgrid (x, x);
%!
%!  phi1 = (XX - 2).^2 + (YY - 2).^2 - 2^2;
%!  phi2 = (XX + 2).^2 + (YY + 2).^2 - 2^2;
%!
%!  assert (~ls_isempty (phi1) && ~ls_isempty (phi2));
%!  assert (ls_isempty (ls_intersect (phi1, phi2)));
%!  assert (~ls_isempty (ls_union (phi1, phi2)));
