##  Copyright (C) 2014  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{where} =} ls_inside (@var{phi})
## 
## Return in a logical array which points are inside the domain
## described by the level-set function @var{phi}.  These are the points
## where @var{phi} is negative.  If the @code{signbit} function is available
## (on newer versions of GNU Octave), also negative zeros are considered
## to be part of the domain.
##
## @seealso{ls_check, ls_issubset, ls_normalise}
## @end deftypefn

function res = ls_inside (phi)
  if (nargin () ~= 1)
    print_usage ();
  endif

  if (exist ("signbit") == 5)
    res = signbit (phi);
  else
    res = (phi < 0);
  endif
endfunction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests.

% Test for error conditions.
%!error <Invalid call to>
%!  ls_inside ()
%!error <Invalid call to>
%!  ls_inside (1, 2)

% Basic test.
%!test
%!  assert (ls_inside ([1, 0, -1, 0, 1]), [false, false, true, false, false]);

% Test negative zeros.
%!test
%!  if (exist ("signbit") == 5)
%!    assert (ls_inside ([-0, 0]), [true, false]);
%!  else
%!    warning ("'signbit' function not available, skipping test.");
%!  endif
