##  Copyright (C) 2014-2015  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Main program for running all the demos.

clear ("all");
page_screen_output (false);
addpath ("src");
addpath ("inst");

demo fastmarching

demo ls_distance_fcn
demo ls_signed_distance
demo ls_solve_stationary
demo ls_extract_solution

demo ls_time_step

demo ls_animate_evolution
demo ls_enforce_speed

demo ls_genbasic
demo ls_union
demo ls_intersect
demo ls_setdiff
demo ls_setxor

demo ls_find_geometry
demo ls_nb_from_geom
demo ls_build_mesh

demo ls_get_tests
demo ls_sign_colourmap

demo so_run_descent
demo so_replay_descent
demo so_explore_descent
