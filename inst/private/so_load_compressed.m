##  Copyright (C) 2015  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{x} =} so_load_compressed (@var{fd}, @var{field}, @var{data})
## 
## Internal routine that loads and optionally ``uncompresses'' data
## from a descent log.  This is used by both @code{so_replay_descent}
## and @code{so_explore_descent} and thus shared here.
##
## @seealso{so_replay_descent, so_explore_descent}
## @end deftypefn

function x = so_load_compressed (fd, field, data)
  x = fload (fd);

  if (isfield (data, "compress") && isfield (data.compress, "load")
        && isfield (data.compress.load, field))
    fcn = getfield (data.compress.load, field);
    x = fcn (x, data);
  endif
endfunction
