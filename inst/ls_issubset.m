##  Copyright (C) 2014  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{res} =} ls_issubset (@var{phi1}, @var{phi2})
## 
## Check if the set described by @var{phi1} is a subset of @var{phi2}.
##
## @seealso{ls_inside, ls_equal, ls_disjoint, ls_check}
## @end deftypefn

function res = ls_issubset (phi1, phi2)
  if (nargin () ~= 2)
    print_usage ();
  endif

  if (~all (size (phi1) == size (phi2)))
    error ("PHI1 and PHI2 must be of the same size");
  endif

  res = ls_check (phi2, "contain", ls_inside (phi1));
endfunction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests.

% Test for error conditions.
%!error <Invalid call to>
%!  ls_issubset (1)
%!error <Invalid call to>
%!  ls_issubset (1, 2, 3)
%!error <PHI1 and PHI2 must be of the same size>
%!  ls_issubset (1, [1, 2])

% Basic tests for the cases.
%!test
%!  n = 50;
%!  x = linspace (-10, 10, n);
%!  [XX, YY] = meshgrid (x, x);
%!
%!  phi1 = XX.^2 + YY.^2 - 1^2;
%!  phi2 = XX.^2 + YY.^2 - 5^2;
%!  phi3 = XX.^2 + YY.^2 - 8^2;
%!  phi4 = (XX - 3).^2 + YY.^2 - 3^2;
%!
%!  assert (ls_issubset (phi1, phi2));
%!  assert (ls_issubset (phi4, phi3));
%!  assert (~ls_issubset (phi4, phi2));
