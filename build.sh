#!/bin/sh

#   GNU Octave level-set package.
#   Copyright (C) 2013-2015  Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Build the package as .tar.gz file.

name=level-set-0.3.1.tar.gz
dirname=level-set

set -e

./autogen.sh

rm -rf /tmp/$dirname
mkdir /tmp/$dirname
cp CITATION COPYING INDEX DESCRIPTION NEWS /tmp/$dirname
cp -r inst/ /tmp/$dirname
mkdir /tmp/$dirname/src
cp src/configure src/Makefile.in src/*.?pp /tmp/$dirname/src

# Also include autoconf-related files, even though they are not strictly
# necessary for installing the package.  It may be useful for others
# to hack on the code.
cp autogen.sh /tmp/$dirname
cp src/configure.ac /tmp/$dirname/src

# Clean up possible Vim swap files or something like that.
find /tmp/$dirname -type f -and -name .\* -exec rm -f {} \;

outfile=`pwd`/$name
(cd /tmp; tar zcvf $outfile $dirname)

rm -rf /tmp/$dirname
