##  Copyright (C) 2014-2015  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{phi} =} ls_complement (@var{phi})
## 
## Construct a level-set function for the complement of the set
## described by @var{phi}.
##
## This operation preserves a signed distance function:  If @var{phi}
## is actually the signed distance function of the set, then also
## the return value will be the signed distance function of the set's
## complement.
##
## @seealso{ls_union, ls_intersect, ls_setdiff, ls_setxor}
## @end deftypefn

function phi = ls_complement (phi)
  if (nargin () ~= 1)
    print_usage ();
  endif

  phi = -phi;
endfunction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests.

% Test for error conditions.
%!error <Invalid call to>
%!  ls_complement ()
%!error <Invalid call to>
%!  ls_complement (1, 2)

% Basic tests for the function.
%!test
%!  n = 50;
%!  x = linspace (-10, 10, n);
%!  h = x(2) - x(1);
%!  [XX, YY] = meshgrid (x, x);
%!
%!  phi1 = (XX - 2).^2 + (YY - 2).^2 - 2^2;
%!  phi2 = (XX + 2).^2 + (YY + 2).^2 - 2^2;
%!
%!  assert (ls_disjoint (phi1, ls_complement (phi1)));
%!  assert (ls_disjoint (phi2, ls_complement (phi2)));
%!
%!  assert (~ls_disjoint (phi1, ls_complement (phi2)));
%!  assert (~ls_disjoint (phi2, ls_complement (phi1)));
%!
%!  sd1 = ls_signed_distance (ls_complement (phi1), h);
%!  sd2 = ls_complement (ls_signed_distance (phi1, h));
%!  assert (sd1, sd2, sqrt (eps));
