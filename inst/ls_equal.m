##  Copyright (C) 2014-2015  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{res} =} ls_equal (@var{phi1}, @var{phi2})
## 
## Check if the sets described by @var{phi1} and @var{phi2} are equal.
## This compares the domains according to @code{ls_inside}, but does not
## interpret magnitudes of the values and thus it does not take into
## account where exactly the ``approximate boundary'' intersects
## @emph{between} grid points.
##
## @seealso{ls_inside, ls_issubset, ls_disjoint, ls_hausdorff_dist}
## @end deftypefn

function res = ls_equal (phi1, phi2)
  if (nargin () ~= 2)
    print_usage ();
  endif

  if (~all (size (phi1) == size (phi2)))
    error ("PHI1 and PHI2 must be of the same size");
  endif

  ins1 = ls_inside (phi1);
  ins2 = ls_inside (phi2);
  res = all (ins1(:) == ins2(:));
endfunction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests.

% Test for error conditions.
%!error <Invalid call to>
%!  ls_equal (1)
%!error <Invalid call to>
%!  ls_equal (1, 2, 3)
%!error <PHI1 and PHI2 must be of the same size>
%!  ls_equal (1, [1, 2])

% Basic test.
%!test
%!  assert (ls_equal ([Inf, 0, -2, -Inf, eps], ...
%!                    [1, 0, -Inf, -eps, 1]));
%!  assert (ls_equal ([1, 0; -1, 0], [Inf, 0; -5, 0]));
%!  assert (!ls_equal ([1, -1; 0, 0], [1, 0; 0, 0]));

% Test involving signed zero.
%!test
%!  if (exist ("signbit") == 5)
%!    assert (ls_equal ([0, -0], [1, -1]));
%!  else
%!    warning ("'signbit' function not available, skipping test.");
%!  endif
