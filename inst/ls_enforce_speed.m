##  Copyright (C) 2014  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{f} =} ls_enforce_speed (@var{f}, @var{type}, @var{where})
## 
## Enforce geometric constraints by changing the
## speed field @var{f} accordingly.  This change ensures that
## the constraint is not violated while evolving a level-set function
## with the new speed field.
##
## @var{type} specifies what the constraint
## should be, and @var{where} should be a logical array of the same size
## as the grid (and thus @var{f}), specifying which grid points are
## part of the set that defines the constraint.
## Possible values for @var{type}:
##
## @table @asis
## @item @qcode{"inside"}
## The domain should be inside the region marked as @var{where}.
##
## @item @qcode{"outside"}
## The domain should not intersect the region marked as @var{where}.
##
## @item @qcode{"contain"}
## The domain should always contain the region marked in @var{where}.
## @end table
##
## @seealso{ls_check, ls_enforce, ls_solve_stationary, ls_extract_solution}
## @end deftypefn

function f = ls_enforce_speed (f, type, where)
  if (nargin () ~= 3)
    print_usage ();
  endif

  if (~all (size (f) == size (where)))
    error ("F and WHERE must be of the same size");
  endif
  if (~strcmp (typeinfo (type), "string"))
    error ("TYPE must be a string");
  endif

  switch (type)
    case "inside"
      f(~where & (f > 0)) = 0;
    case "outside"
      f(where & (f > 0)) = 0;
    case "contain"
      f(where & (f < 0)) = 0;
    otherwise
      error ("invalid value '%s' for TYPE argument", type);
  endswitch
endfunction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests.

% Test for error conditions.
%!error <Invalid call to>
%!  ls_enforce_speed (1, 2)
%!error <Invalid call to>
%!  ls_enforce_speed (1, 2, 3, 4)
%!error <F and WHERE must be of the same size>
%!  ls_enforce_speed (1, "inside", [1, 2]);
%!error <invalid value 'foo' for TYPE argument>
%!  ls_enforce_speed (1, "foo", true);
%!error <TYPE must be a string>
%!  ls_enforce_speed (1, NA, false);

% For the following test, define a utility function that
% checks whether a given domain is (approximately) a circle
% with given radius.
%!function checkCircleRadius (XX, YY, h, phi, r)
%!  phiCorrect = XX.^2 + YY.^2 - r^2;
%!  sd1 = ls_signed_distance (phi, h);
%!  sd2 = ls_signed_distance (phiCorrect, h);
%!  assert (sd1, sd2, h);
%!endfunction

% Shrink and grow a circle, touching constraints.  This checks that the
% constraints are correctly enforced and also that non-applicable constraints
% do not interfere with the speed.
%!test
%!  n = 100;
%!  x = linspace (-10, 10, n);
%!  h = x(2) - x(1);
%!  [XX, YY] = meshgrid (x, x);
%!
%!  phi = XX.^2 + YY.^2 - 3^2;
%!  F = ones (size (XX));
%!  whereContain = (XX.^2 + YY.^2 < 5^2);
%!  F = ls_enforce_speed (F, "contain", whereContain);
%!  whereInside = (XX.^2 + YY.^2 < 8^2);
%!  F = ls_enforce_speed (F, "inside", whereInside);
%!  dists = ls_solve_stationary (phi, F, h);
%!  phi2 = ls_extract_solution (10, dists, phi, F);
%!  assert (ls_check (phi2, "contain", whereContain));
%!  assert (ls_check (phi2, "inside", whereInside));
%!  checkCircleRadius (XX, YY, h, phi2, 8);
%!
%!  phi = XX.^2 + YY.^2 - 8^2;
%!  F = -ones (size (XX));
%!  whereContain = (XX.^2 + YY.^2 < 5^2);
%!  F = ls_enforce_speed (F, "contain", whereContain);
%!  whereOutside = (XX.^2 + YY.^2 >= 7^2);
%!  F = ls_enforce_speed (F, "outside", whereOutside);
%!  dists = ls_solve_stationary (phi, F, h);
%!  phi2 = ls_extract_solution (10, dists, phi, F);
%!  assert (ls_check (phi2, "contain", whereContain));
%!  assert (ls_check (phi2, "outside", whereOutside));
%!  checkCircleRadius (XX, YY, h, phi2, 5);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Demos.

% Shrink and grow a circle.
%!demo
%!  n = 100;
%!  x = linspace (-10, 10, n);
%!  h = x(2) - x(1);
%!  [XX, YY] = meshgrid (x, x);
%!
%!  phi = XX.^2 + YY.^2 - 8^2;
%!  F = -ones (size (XX));
%!  F = ls_enforce_speed (F, "outside", XX.^2 + YY.^2 >= 7^2);
%!  F = ls_enforce_speed (F, "contain", XX.^2 + YY.^2 < 3^2);
%!  times = linspace (1, 6, 40);
%!  ls_animate_evolution (phi, F, h, times, 0.05);
%!
%!  phi = XX.^2 + YY.^2 - 3^2;
%!  F = ones (size (XX));
%!  F = ls_enforce_speed (F, "inside", XX.^2 + YY.^2 < 8^2);
%!  F = ls_enforce_speed (F, "contain", XX.^2 + YY.^2 < 5^2);
%!  ls_animate_evolution (phi, F, h, times, 0.05);
